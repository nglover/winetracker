using System;
using System.IO;
using System.Linq;
using DataModels;
using Xunit;

namespace Tests
{
    public class EmailParserTests
    {
        [Fact]
        public void Given_Email_Doesnt_Contain_Expected_WineSocietyDetails_Expect_Exception()
        {
            //Arrange
            var sut = new EmailParser.ForwardedEmailParser();
            var email = File.ReadAllText(@"EmailParser\NotRecgonisedAsWineSociety.txt");

            //Act
            var exception = Assert.Throws<Exception>(() => sut.ParseEmail(email));
            
            //Assert
            Assert.Equal("Not recognised as wine society email", exception.Message);

        }

        [Fact]
        public void Given_Invalid_Table_Headers_Expect_Exception()
        {
            //Arrange
            var sut = new EmailParser.ForwardedEmailParser();
            var email = File.ReadAllText(@"EmailParser\InvalidHeader.txt");

            //Act
            var exception = Assert.Throws<Exception>(() => sut.ParseEmail(email));
            
            //Assert
            Assert.True(exception.Message.StartsWith("Headers don't match expected format"));
        }

        [Fact]
        public void Given_Email_Expect_Can_Parse()
        {
            //Arrange
            var sut = new EmailParser.ForwardedEmailParser();
            var email = File.ReadAllText(@"EmailParser\HtlmFragment.txt");
            var expectedOrderDate = new DateTime(2017, 6, 12);
            var expectedWines = new[]
            {
                new WineModel("8522235", expectedOrderDate, "CE9041",
                    "The Society&#39;s Chilean Sauvignon Blanc Limari  2016", 7.75),
                new WineModel("8522235", expectedOrderDate, "BG251",
                    "Soli Pinot Noir Edoardo Miroglio 2015", 8.95),
                new WineModel("8522235", expectedOrderDate, "FC32081",
                    "Minervois Chateau Sainte Eulalie Rose 2016", 8.5),
                new WineModel("8522235", expectedOrderDate, "SP12391",
                    "Senorio de Sarria Rosado Navarra 2016", 6.5),
                new WineModel("8522235", expectedOrderDate, "AR3661",
                    "Las Piletas Malbec Special selection for The Wine Society 2015", 9.95),
                new WineModel("8522235", expectedOrderDate, "AU18991",
                    "Billi Billi Grampians Shiraz 2013", 9.50),
                new WineModel("8522235", expectedOrderDate, "IT22981",
                    "Chianti Rufina Frascole 2014", 11.50),
                new WineModel("8522235", expectedOrderDate, "CE9131",
                    "Primus Colchagua Carmenere 2014", 11.95),
                new WineModel("8522235", expectedOrderDate, "CE9301",
                    "Koyle Costa Cuarzo Rapel Coastal Sauvignon Blanc 2016", 7.95),
                new WineModel("8522235", expectedOrderDate, "CE9451",
                    "Tabali Vetas Blancas Limari Syrah 2013", 12.50),
                new WineModel("8522235", expectedOrderDate, "CE9481",
                    "Tabali Reserva Limited Edition Peumo Carmenere 2014", 12.50),
                new WineModel("8522235", expectedOrderDate, "CE8761",
                    "Maycas del Limari Reserva Especial Pinot Noir 2014", 12.95),
                new WineModel("8522235", expectedOrderDate, "NZ9181",
                    "Wither Hills Marlborough Sauvignon Blanc 2016", 8.50)
            };

            //Act
            var actualWineModels = sut.ParseEmail(email);

            //Assert
            Assert.True(expectedWines.SequenceEqual(actualWineModels));
        }
    }
}
