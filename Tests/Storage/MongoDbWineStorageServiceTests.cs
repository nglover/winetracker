﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using Storage;
using Storage.MongoDb;
using Xunit;

namespace Tests.Storage
{
    public class MongoDbWineStorageServiceTests
    {
        public IWineStorageService GetStorageService()
        {
            return new MongoDbWineStorageService(new MongoDbSettings("localhost"));
        }

        public IWineStorageService GetCosmosStorageService()
        {
            return new MongoDbWineStorageService(new CosmosMongoDbSettings());
        }

        [Fact(Skip = "Intergration")]
        //[Fact]
        public async Task Given_Wines_Expect_Stored()
        {
            //Arrange
            var sut = GetStorageService();

            //Act
            var result = await sut.StoreWinesAsync(new[]
            {
                new WineModel("123", new DateTime(2017, 6, 10), "Code1", "Best Wine Ever", 25.99),
                new WineModel("123", new DateTime(2017, 6, 10), "Code2", "Every Day Wine", 9.99)
            });

            //Assert
            Assert.True(result);
        }

        [Fact(Skip = "Intergration")]
        //[Fact]
        public async Task Given_Wines_Expect_Can_Store_InCosmosDb()
        {
            //Arrange
            var sut = GetCosmosStorageService();

            //Act
            var result = await sut.StoreWinesAsync(new[]
            {
                new WineModel("123", new DateTime(2017, 6, 10), "Code1", "Best Wine Ever", 25.99),
                new WineModel("123", new DateTime(2017, 6, 10), "Code2", "Every Day Wine", 9.99)
            });

            //Assert
            Assert.True(result);
        }
    }
}
