﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Text;
using MongoDB.Driver;

namespace Storage.MongoDb
{
    public class CosmosMongoDbSettings : IMongoDbSettings
    {
        public CosmosMongoDbSettings()
        {
            var connection = Environment.GetEnvironmentVariable("WineTrackerMongoDbConnectionStr");
            var settings = MongoClientSettings.FromUrl(new MongoUrl(connection));
            settings.SslSettings = new SslSettings()
            {
                EnabledSslProtocols = SslProtocols.Tls12
            };

            MongoSettings = settings;
        }

        public MongoClientSettings MongoSettings { get; }
    }
}
