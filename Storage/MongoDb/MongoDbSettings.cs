﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;

namespace Storage.MongoDb
{
    public class MongoDbSettings : IMongoDbSettings
    {
        public MongoDbSettings(string connectionString)
        {
            MongoSettings = new MongoClientSettings {Server = new MongoServerAddress(connectionString)};
        }
        
        public MongoClientSettings MongoSettings { get; }
    }
}
