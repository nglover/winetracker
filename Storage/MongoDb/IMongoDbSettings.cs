﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;

namespace Storage.MongoDb
{
    public interface IMongoDbSettings
    {
        MongoClientSettings MongoSettings { get; }
    }
}
