﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataModels;
using MongoDB.Driver;

namespace Storage.MongoDb
{
    public class MongoDbWineStorageService : IWineStorageService
    {
        private readonly IMongoDatabase _wineTrackerDatabase;

        public MongoDbWineStorageService(IMongoDbSettings mongoDbSettings)
        {
            MongoClient client = new MongoClient(mongoDbSettings.MongoSettings);
            _wineTrackerDatabase = client.GetDatabase("WineTracker");
        }

        public async Task<bool> StoreWinesAsync(IEnumerable<WineModel> wines)
        {
            var wineCollection =_wineTrackerDatabase.GetCollection<WineModel>("Wines");
            await wineCollection.InsertManyAsync(wines);
            return true;
        }
    }
}
