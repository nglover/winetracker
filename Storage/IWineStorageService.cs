﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataModels;

namespace Storage
{
    public interface IWineStorageService
    {
        Task<bool> StoreWinesAsync(IEnumerable<WineModel> wines);
    }
}
