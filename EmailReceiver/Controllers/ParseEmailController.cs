﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmailParser;
using EmailReceiver.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Storage;

namespace EmailReceiver.Controllers
{
    [Route("api/[controller]")]
    public class ParseEmailController : Controller
    {
        private readonly ILogger _logger;
        private readonly IEmailParser _emailParser;
        private readonly IWineStorageService _wineStorageService;

        public ParseEmailController(
            ILogger<ParseEmailController> logger, 
            IEmailParser emailParser,
            IWineStorageService wineStorageService)
        {
            _logger = logger;
            _emailParser = emailParser;
            _wineStorageService = wineStorageService;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
           _logger.LogInformation(1, "Getting Values");
            return new string[] { "value1", "value2" };
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> RecieveEmailAsync([FromBody]MailInModel mailInModel)
        {
            _logger.LogInformation($"Received New Email, Preparing to Parse...");

            try
            {
                var wines = _emailParser.ParseEmail(mailInModel.Html).ToArray();
                _logger.LogInformation($"Email contained {wines.Count()} wine under order number {wines.FirstOrDefault()?.OrderNumber}");
                await _wineStorageService.StoreWinesAsync(wines);
                _logger.LogInformation("Wine are now persisted in storage");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Unexpected error parsing email {ex}");
                return BadRequest(ex);
            }

            return Accepted();
        }
    }
}
