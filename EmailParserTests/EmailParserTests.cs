﻿using System;
using System.IO;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace EmailParserTests
{
    [TestFixture]
    public class EmailParserTests
    {
        [Test]
        public void Given_Email_Expect_Can_Parse()
        {
            //Arrange
            var sut = new EmailParser.EmailParser();
            var email = File.ReadAllText(@"HtmlFragment.txt");

                //Act
            var actualEmail = sut.ParseEmail(email);

            //Assert
        }
    }
}
