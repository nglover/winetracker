﻿using System;

namespace DataModels
{
    public class WineModel : IEquatable<WineModel>
    {
        public WineModel(string orderNumber, DateTime orderDate, string code, string description, double price)
        {
            OrderNumber = orderNumber;
            OrderDate = orderDate;
            Code = code;
            Description = description;
            Price = price;
        }

        public string OrderNumber { get; }

        public DateTime OrderDate { get; }

        public string Code { get; }

        public string Description { get; }

        public double Price { get; }

        public bool Equals(WineModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(OrderNumber, other.OrderNumber) && OrderDate.Equals(other.OrderDate) && string.Equals(Code, other.Code) && string.Equals(Description, other.Description) && Price.Equals(other.Price);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((WineModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (OrderNumber != null ? OrderNumber.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ OrderDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (Code != null ? Code.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Price.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"Code {Code}, Desc {Description}, Price {Price}";
        }
    }
}
