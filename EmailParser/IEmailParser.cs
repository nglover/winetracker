﻿using System.Collections.Generic;
using DataModels;

namespace EmailParser
{
    public interface IEmailParser
    {
       IEnumerable<WineModel> ParseEmail(string html);
    }
}
