﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModels;
using HtmlAgilityPack;

namespace EmailParser
{
    public class ForwardedEmailParser : IEmailParser
    {
        private const string Quantity = "Quantity";
        private const string Code = "Code";
        private const int CodeIndex = 1;
        private const string Name = "Name";
        private const int NameIndex = 2;
        private const string Price = "Price/£";
        private const int PriceIndex = 3;
        private const string Total = "Total/£";

        public IEnumerable<WineModel> ParseEmail(string html)
        {
            var doc = LoadIntoHtmlDocument(html);
            ValidateIsWineSocietyEmail(doc);

            var orderNumber = GetAndValidateOrderNumber(doc);
            var orderDate = GetAndValidateOrderDate(doc);

            var allRows = GetWineOrderRows(doc);
            var header = allRows.Nodes().FirstOrDefault();
            var wines = allRows.Nodes().Skip(1);

            ValidateTableHeaders(header);

            var actualWineNodes = wines?.Where(n => n.ChildNodes.All(cn => !string.IsNullOrWhiteSpace(cn.InnerText)));

            return actualWineNodes.Select(n =>
            {
                var code = n.ChildNodes[CodeIndex].InnerText;
                var name = n.ChildNodes[NameIndex].InnerText;
                var price = Convert.ToDouble(n.ChildNodes[PriceIndex].InnerText);
                return new WineModel(orderNumber, orderDate, code, name, price);
            });
        }

        private DateTime GetAndValidateOrderDate(HtmlDocument html)
        {
            var brTags = html.DocumentNode.SelectNodes("//br");
            var orderDate = "Date: ";

            var possibleMatches = brTags.Where(t => !string.IsNullOrEmpty(t.NextSibling?.InnerText) && t.NextSibling.InnerText.Contains(orderDate));
            var lastMatch = possibleMatches.LastOrDefault();

            if (lastMatch == null)
                throw new Exception("No order date found");

            var dateStr = lastMatch.NextSibling.InnerText.Replace(orderDate, "").Split(new []{"at"}, StringSplitOptions.RemoveEmptyEntries);
            var datePart = dateStr[0];

            return Convert.ToDateTime(datePart);
        }

        private string GetAndValidateOrderNumber(HtmlDocument html)
        {
            var brTags = html.DocumentNode.SelectNodes("//br");
            var orderNumber = "Order number: ";

            var possibleMatches = brTags.Where(t => !string.IsNullOrEmpty(t.NextSibling?.InnerText) && t.NextSibling.InnerText.Contains(orderNumber));
            var lastMatch = possibleMatches.LastOrDefault();

            if (lastMatch == null)
                throw new Exception("No order number found");

            return lastMatch.NextSibling.InnerText.Replace(orderNumber, "");
        }

        private void ValidateIsWineSocietyEmail(HtmlDocument html)
        {
            var anchorTags = html.DocumentNode.SelectNodes("//a");
            if (anchorTags == null || !anchorTags.Any(tag => tag.InnerText.Contains("memberservices@thewinesociety.com")))
                throw new Exception("Not recognised as wine society email");
        }

        private static void ValidateTableHeaders(HtmlNode header)
        {
            var expectedHeaders = new[] {Quantity, Code, Name, Price, Total};
            var actualHeaders = header?.ChildNodes.Select(n => n.InnerText);
            if (!expectedHeaders.SequenceEqual(actualHeaders))
                throw new Exception(
                    $"Headers don't match expected format. Expected {string.Join(",", expectedHeaders)} but Recieved {string.Join(",", actualHeaders)}");
        }

        private static HtmlNodeCollection GetWineOrderRows(HtmlDocument html)
        {
            var allRows = html.DocumentNode.SelectNodes("//table//tbody//tr//table//tbody");
            return allRows;
        }

        private static HtmlDocument LoadIntoHtmlDocument(string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc;
        }
    }
}
