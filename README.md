[![Build status](https://ci.appveyor.com/api/projects/status/abs07ke9hnf8ah2s?svg=true)](https://ci.appveyor.com/project/ndglover/winetracker)
[![codecov](https://codecov.io/bb/nglover/winetracker/branch/master/graph/badge.svg)](https://codecov.io/bb/nglover/winetracker)

# Wine Tracker #

Wine Tracker is under development.

The expectation is Wine Tracker will provide a number of features, the principle one being a website were you can see the wines you like.
Right now you can send a confirmation email from the Wine Society and it will stores those wines in the cloud. 
Once the user interface is complete you will be able to rate those wines.

